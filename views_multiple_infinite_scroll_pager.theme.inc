<?php

/**
 * @file
 * 
 * Contains the theme callbacks for our module
 */

/**
 * Theme callback for 'views_multiple_infinite_scroll'
 */
function theme_views_multiple_infinite_scroll_pager($vars) {
    global $pager_page_array, $pager_total;

  $tags = $vars['tags'];
  $element = $vars['element'];
  $parameters = $vars['parameters'];

  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  if ($pager_total[$element] > 1) {

    $li_previous = theme('pager_previous', 
      array(
      'text' => (isset($tags[1]) ? $tags[1] : t('<<')), 
      'element' => $element, 
      'interval' => 1, 
      'parameters' => $parameters,
    )
    );
    if (empty($li_previous)) {
      $li_previous = "&nbsp;";
    }

    $li_next = theme('pager_next', 
      array(
      'text' => (isset($tags[3]) ? $tags[3] : t('>>')), 
      'element' => $element, 
      'interval' => 1, 
      'parameters' => $parameters,
    )
    );

    if (empty($li_next)) {
      $li_next = "&nbsp;";
    }

    $items[] = array(
      'class' => array('pager-previous'), 
      'data' => $li_previous,
    );

    $items[] = array(
      'class' => array('pager-current'), 
      'data' => t('@current of @max', array('@current' => $pager_current, '@max' => $pager_max)),
    );

    $items[] = array(
      'class' => array('pager-next'), 
      'data' => $li_next,
    );
    drupal_add_js(drupal_get_path('module', 'views_multiple_infinite_scroll_pager') . '/js/jquery.infinitescroll.js');
    drupal_add_js(drupal_get_path('module', 'views_multiple_infinite_scroll_pager') . '/js/infinitescroll.config.js');
    return theme('item_list', 
      array(
      'items' => $items, 
      'title' => NULL, 
      'type' => 'ul', 
      'attributes' => array('class' => array('pager','multi_infinite_scroll_pager')),
    )
    );
  }
}
